# Ejercicios sesión 4

## Ejercicios guiados

1. Realiza el siguiente ejercicio guiado para familiarizarte con docker compose: https://docs.docker.com/compose/gettingstarted/

1. Revisar diferentes proyectos: https://github.com/docker/awesome-compose

1. Explora las diferentes etiquetas disponibles para el fichero yml: https://docs.docker.com/compose/compose-file/compose-file-v3/

1. Explorar docker compose CLI: https://docs.docker.com/compose/reference/

## Ejercicios propuestos:

1. Crea un fichero compose.yml para el entorno de desarrollo del proyecto todolist.

1. A partir del fichero docker-compose.yml, crea un fichero     compose.prod.yml para el entorno de producción sin utilizar la etiqueta [build](https://docs.docker.com/compose/compose-file/compose-file-v3/#build); usa [image](https://docs.docker.com/compose/compose-file/compose-file-v3/#image) en su lugar.

1. Revisa el código del fichero [.gitlab-ci.yml](https://gitlab.com/curso_docker_2401/examples/svc-nginx/-/blob/main/.gitlab-ci.yml?ref_type=heads) del proyecto svc-nginx. A continuación:
    1. Crea un fichero `.gitlab-ci.yml` en el proyecto todo-front similar al del proyecto de svc-nginx modificando el namespace del nombre de la imagen. 
    1. Obten un token de acceso para tu imagenes de docker hub. Para ello, entra a `My account > Security > New Access token`.
    1. Crea las variables CI_REGISTRY_USER y CI_REGISTRY_PASSWORD en el repositorio todo-front. Para ello, accede a `Settings > CI/CD > Variables > Add variable`.
    1. Verifica que con cada commit en main sobre el proyecto, se crean dos imágenes en Docker Hub, una con la etiqueta del hash de tu commit y otra latest. Puedes ver el proceso de construcción entrando en la vista `Build > Pipelines` de tu proyecto.

## Ejercicios para ampliar

1. Intenta crear un proyecto que necesite una dependencia externa de un repositorio privado. Puedes intentar añadir al servicio `todo-frontend` la dependencia [npm-component](https://gitlab.com/curso_docker_2401/examples/npm-component).
    - Revisa la documentación de [Gitlab Artifacts](https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html#from-a-url-1) para obtener el artefacto desde URL.

1. Docker Compose tiene la capacidad de trabajar con multiples ficheros compose.yml. Refactoriza los ficheros compose.yml y compose.prod.yml haciendo uso de esta [característica](https://docs.docker.com/compose/multiple-compose-files/extends/).

1. Docker Compose tiene la capacidad de trabajar con [variables de entorno](https://docs.docker.com/compose/environment-variables/). Crea las variables del entorno de producción y el de desarrollo que consideres necesarias. 