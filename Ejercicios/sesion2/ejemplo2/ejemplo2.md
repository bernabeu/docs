
# Ejemplo 2: Construcción de imágenes con una una aplicación PHP

En este ejemplo vamos a crear una imágen con una página desarrollada con PHP. Vamos a crear dos versiones de la imagen, y puedes encontrar los ficheros en este mismo directorio.

## Versión 1: Desde una imagen base

En el contexto vamos a tener el fichero `Dockerfile` y un directorio, llamado `app` con nuestra aplicación.

En este caso vamos a usar una imagen base de un sistema operativo sin ningún servicio. El fichero `Dockerfile` será el siguiente:

```Dockerfile
FROM debian
EXPOSE 80
RUN apt-get update \
 && apt-get install -y apache2 libapache2-mod-php7.3 php7.3 \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*
ADD app /var/www/html/
RUN rm /var/www/html/index.html
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
```

Para crear la imagen ejecutamos:

```bash
$ docker build -t jpaniorte/ejemplo2:v1 .
```

Comprobamos que la imagen se ha creado:

```bash
$ docker images
REPOSITORY             TAG                 IMAGE ID            CREATED             SIZE
jpaniorte/ejemplo2     v1                  8c3275799063        1 minute ago      226MB
```

Y podemos crear un contenedor:

```bash
$ docker run -d -p 80:80 --name ejemplo2 jpaniorte/ejemplo2:v1
```

Y acceder con el navegador a nuestra página a través de http://localhost

La aplicación tiene un fichero `info.php`que me da información sobre PHP, en este caso observamos que estamos usando la versión 7.3:

http://localhost/info.php


## Versión 2: Desde una imagen con PHP instalado

En este caso el fichero `Dockerfile` sería el siguiente:

```Dockerfile
EXPOSE 80
FROM php:7.4-apache
ADD app /var/www/html/
```

En este caso no necesitamos instalar nada, ya que la imagen tiene instalado el servidor web y PHP. No es necesario indicar el `CMD` ya que por defecto el contenedor creado a partir de esta imagen ejecutará el mismo proceso que la imagen base, es decir, la ejecución del servidor web.

De forma similar, crearíamos una imagen y un contenedor:

```bash
$ docker build -t jpaniorte/ejemplo2:v2 .
$ docker run -d -p 80:80 --name ejemplo2 jpaniorte/ejemplo2:v2
```

Podemos acceder al fichero `info.php` para comprobar la versión de php que estamos utilizando con esta imagen:

![ejemplo2](img/ejemplo2_phpinfo2.png)
