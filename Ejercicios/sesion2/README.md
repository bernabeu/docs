# Ejercicios sesión 2

## Ejercicios guiados

1. [recordatorio](recordatorio.md): Crear una imagen a partir de un contenedor en ejecución.
2. [hello world](hello_world.md): Primer ejercicio con Dockerfile.
3. Hacer al menos uno:
    1. [DebianApache + Apache + Nginx](./ejemplo1/ejemplo1.md) *recomendado
    2. [DebianPhp + Php](./ejemplo2/ejemplo2.md)
    3. [Python](./ejemplo3/ejemplo3.md)


## Proyectos propuestos

### Configuración del espacio de trabajo

> Docuementación sobre forks:
>
> https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html

1. Haz un fork del proyecto a tu espacio

![img](../../img/fork_img/un.png)

2. Selecciona los parámetros que consideres:

![img](../../img/fork_img/dos.png)

3. Tu proyecto estará 'forkeado'

![img](../../img/fork_img/tres.png)

4. Clona el proyecto en tu equipo

![img](../../img/fork_img/cuatre.png)

```bash
$ git clone <project_url>
```

5. Abre en tu IDE favorito o tu editor de texto el proyecto y crea un fichero vacío `Dockerfile`.

6. Edita el fichero `Dockerfile` con las instrucciones que consideres y ejecuta para generar una nueva imagen:

```bash
docker build -t <nombre_imagen> .
```


| Proyecto    | Dificultad |
| -------- | ------- |
| 1. [Leer la documentación](https://gitlab.com/curso_docker_2401/docs#como-trabajar-con-los-ejemplos)   |     |
| 2. [Telnet_container](https://gitlab.com/curso_docker_2401/examples/telnet_container) | :diamonds:    |
| 3. [Http_server_container](https://gitlab.com/curso_docker_2401/examples/http_server_container)     | :diamonds: :diamonds: |
| 4. [Vue3](https://gitlab.com/curso_docker_2401/examples/vue-3)  |  :diamonds:      |
| 5. [Lumen-10](https://gitlab.com/curso_docker_2401/examples/lumen-10)    |  :diamonds:     |
| 6. [svc-nginx](https://gitlab.com/curso_docker_2401/examples/svc-nginx)  | :diamonds: :diamonds:     |
| 7. [svc-tasks](https://gitlab.com/curso_docker_2401/examples/svc-tasks)    | :diamonds: :diamonds:    |
| 8. [svc-auth](https://gitlab.com/curso_docker_2401/examples/svc-auth) | :diamonds: :diamonds:     |
| 9. [svc-front](https://gitlab.com/curso_docker_2401/examples/svc-front)    |  :diamonds: :diamonds: :diamonds:    |