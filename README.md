# Curso Docker 2024 01

## Trasparencias

0. [Presentación](./powerPoint/0_presentacion.pptx) 
1. [Fundamentos](./powerPoint/1_fundamentos.pptx)
2. [Arquitectura + Dockerfile](./powerPoint/2_arq_dockerfiles.pptx)

## Ejercicios

1. [Sesión 1](./Ejercicios/sesion1/ejercicios.md)
2. [Sesión 2](./Ejercicios/sesion2/main.md)

## Proyectos de ejemplo

- [Repo](https://gitlab.com/curso_docker_2401/examples)

## Documentación de referencia

1. [Instalación](https://docs.docker.com/get-docker/)
2. [Docker CheatSheet](./docker_cheatsheet.pdf)
3. [Docker CLI](https://docs.docker.com/engine/reference/commandline/cli/)
4. [Docker Run](https://docs.docker.com/engine/reference/run/)
5. [Docker Build](https://docs.docker.com/build/)


## Enlaces de interés
1. [Libro: Aprende Docker](https://amzn.eu/d/46jATcr)
2. [Curso @josedom24](https://iesgn.github.io/curso_docker_2021/)
3. [Ansible+Vagrant+Examples](https://github.com/geerlingguy/ansible-vagrant-examples/tree/master)
4. [Kubernetes 101](https://github.com/geerlingguy/kubernetes-101)
5. [+ Docker examples](https://github.com/geerlingguy/docker-examples)
